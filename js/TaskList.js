class TaskList extends React.Component {
    renderTask(task) {
        return (
            <Task
                key={task.id}
                onClick={(x, y) => this.props.onClickTask(task.id, x, y)}
                onEdit={(name, desc) => this.props.onEditTask(task.id, name, desc)}
                onRemove={() => this.props.onRemoveTask(task.id)}
                dragged={task.id == this.props.currentlyDraggedTask}
                currentlyDraggedTaskX={this.props.currentlyDraggedTaskX}
                currentlyDraggedTaskY={this.props.currentlyDraggedTaskY}
                task={task}
            />
        );
    }
  
    render() {
        const tasks = this.props.tasks.map((task) => {
            return this.renderTask(task);
        });

        return (
            <div className="task-list">
                <ul>
                    <li>{tasks}</li>
                </ul>
            </div>
        );
    }
}