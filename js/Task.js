class Task extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editMode: false,
            temporaryName: "",
            temporaryDescription: "",
        };
    }
    
    isDragged() {
        return this.props.dragged;
    }
    
    isInEditMode() {
        return this.state.editMode;
    }
    
    onClick(event) {
        if(!this.isInEditMode()) {
            this.props.onClick(event.clientX, event.clientY);
        }
    }
    
    startEditing() {
        if(!this.isDragged()) {
            this.setState({
                editMode: true,
                temporaryName: this.props.task.name,
                temporaryDescription: this.props.task.description,
            });
        }
    }
    
    saveChanges(event) {
        event.preventDefault();
        this.props.onEdit(this.state.temporaryName, this.state.temporaryDescription);
        this.setState({editMode: false});
    }
    
    handleNameChange(name) {
        this.setState({ temporaryName: name });
    }
  
    handleDescriptionChange(description) {
        this.setState({ temporaryDescription: description });
    }

    renderNormal() {
        let style = {};
        
        if(this.props.dragged) {
            style.left = this.props.currentlyDraggedTaskX;
            style.top = this.props.currentlyDraggedTaskY;
        }
        
        return (
        <div style={style} className={`task ${this.props.dragged ? 'dragged' : ''}`}>
            <span
                className="name"
                onMouseDown={e => this.onClick(e)}
            >
                #{this.props.task.id}/{this.props.task.index} {this.props.task.name}
            </span>
            <p className="description">{this.props.task.description}</p>
            <div className="buttons">
                <button onClick={() => this.startEditing()}>Edit</button>
                <button className="remove" onClick={() => this.props.onRemove()}>Remove</button>
            </div>
        </div>
        );
    }
    renderInEditMode() {
        return (
            <form className='task' onSubmit={e => this.saveChanges(e)}>
                <input
                    className="name"
                    value={this.state.temporaryName}
                    onChange={e => this.handleNameChange(e.target.value)}
                >
                </input>
                <textarea
                    className="description"
                    value={this.state.temporaryDescription}
                    onChange={e => this.handleDescriptionChange(e.target.value)}
                />
                <div className="buttons">
                    <button type='submit'>Save</button>
                    <button className="remove" onClick={() => this.props.onRemove()}>Remove</button>
                </div>
            </form>
        );
    }
    render() {
        if(this.isInEditMode())
            return this.renderInEditMode();
        else
            return this.renderNormal();
    }
}