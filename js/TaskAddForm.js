class TaskAddForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            taskName: "",
            taskDescription: ""
        };
    }
  
    addTask() {
        const name = this.refs.nameInput.value;
        const description = this.refs.descriptionInput.value;

        if (name != "") {
            this.props.onAddTask(name, description);
        }
    }
  
    handleNameChange(name) {
        this.setState({ taskName: name });
    }
  
    handleDescriptionChange(description) {
        this.setState({ taskDescription: description });
    }
  
    render() {
        return (
            <div className="task-add-form">
                <input
                    ref="nameInput"
                    placeholder="Name"
                    onChange={e => this.handleNameChange(e.target.value)}
                    value={this.state.taskName}
                />
                <textarea
                    ref="descriptionInput"
                    placeholder="Description"
                    onChange={e => this.handleDescriptionChange(e.target.value)}
                    value={this.state.taskDescription}
                />
                <div className="buttons">
                    <button onClick={() => this.addTask()}>Add</button>
                </div>
            </div>
        );
    }
}