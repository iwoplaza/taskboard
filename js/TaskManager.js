function findFreeTaskId(tasks) {
    let freeId = 0;
    while(true) {
        let free = true;
        for(let i = 0; i < tasks.length; ++i) {
            if(tasks[i].id == freeId) {
                free = false;
                break;
            }
        }
        if(free)
            break;
        else
            freeId++;
    }
    return freeId;
}

function findTaskWithId(tasks, id)
{
    let found = tasks.filter(x => x.id == id);
    return found.length == 0 ? null : found[0];
}

class TaskManager extends React.Component {
    constructor(props) {
        super(props);

        let tasks = [];
        const storedTasks = localStorage.getItem("tasks");
        if(storedTasks) {
            tasks = JSON.parse(storedTasks);
        } else {
            tasks = [
                {
                    id: 0,
                    index: 0,
                    name: "Do chores",
                    description: "Didn't you hear what I said?!"
                }
            ];
        }

        this.state = {
            tasks: tasks,
            currentlyDraggedTask: null,
            currentlyDraggedTaskX: 0,
            currentlyDraggedTaskY: 0,
        };
    }
  
    componentDidMount() {
        window.addEventListener('mousemove', e => this.handlePageMouseMove(e), false);
        window.addEventListener('mouseup', e => this.handlePageMouseUp(e), false);
    }

    saveSession() {
        const tasks = this.state.tasks;
        localStorage.setItem("tasks", JSON.stringify(tasks));
    }
    
    handleEditTask(id, name, description) {
        console.log('Changing '+id);
        const tasks = this.state.tasks.slice();

        let task = findTaskWithId(tasks, id);
        if(task != null) {
            task.name = name;
            task.description = description;
            
            this.setState({
                tasks: tasks
            });
        }
    }
    
    handleRemoveTask(id) {
        const tasks = this.state.tasks.slice();

        let task = findTaskWithId(tasks, id);
        console.log('Removing '+id+": "+task);
        if(task != null) {
            tasks.splice(task.index, 1);

            // Updating indices
            for(let i = task.index; i < tasks.length; ++i) {
                tasks[i].index = i;
            }

            this.setState({
                tasks: tasks
            });
        }
    }
  
    handleAddTask(name, description) {
        const tasks = this.state.tasks.slice();

        let freeId = findFreeTaskId(tasks);

        let task = {
            id: freeId,
            index: tasks.length,
            name: name,
            description: description,
        };

        this.setState({
            tasks: tasks.concat([task]),
        });
    }
    
    handleClickTask(id, x, y) {
        this.setState({
            currentlyDraggedTask: id,
            currentlyDraggedTaskX: x,
            currentlyDraggedTaskY: y,
        });
    }
    
    handlePageMouseMove(event) {
        if(this.state.currentlyDraggedTask != null) {
            this.setState({
                currentlyDraggedTaskX: event.clientX,
                currentlyDraggedTaskY: event.clientY,
            });
        }
    }
    
    handlePageMouseUp(event) {
        if(this.state.currentlyDraggedTask != null) {
            this.setState({
                currentlyDraggedTask: null
            });
        }
    }
  
    handleClear() {
        this.setState({tasks: []});
    }

    render() {
        this.saveSession();

        return (
            <div className="task-manager">
                <h1>Tasks</h1>
                <button onClick={() => this.handleClear()}>Clear</button> 
                <TaskAddForm
                    onAddTask={(name, description) =>
                        this.handleAddTask(name, description)
                    }
                />
                <TaskList
                    tasks={this.state.tasks}
                    currentlyDraggedTask={this.state.currentlyDraggedTask}
                    currentlyDraggedTaskX={this.state.currentlyDraggedTaskX}
                    currentlyDraggedTaskY={this.state.currentlyDraggedTaskY}
                    onClickTask={(id, x, y) => this.handleClickTask(id, x, y)}
                    onEditTask={(id, name, desc) => this.handleEditTask(id, name, desc)}
                    onRemoveTask={id => this.handleRemoveTask(id)}
                />
            </div>
        );
    }
}